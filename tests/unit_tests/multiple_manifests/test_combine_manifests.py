#
# Copyright 2019 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest.mock

from distribution_package.utility.multiple_manifests import combine_manifests

import distribution_package.utility.multiple_manifests

class TestCombineManifests( unittest.TestCase ) :

    def test_multiple_yaml_union(self) :
        """
        Test that data from multiple manifests is aggregated correctly.
        """
        expected_yaml_data = [
            {
                'include' : {
                    'files' : [ 'LICENSE', 'README.md' ],
                },
            },
            {
                'exclude' : {
                    'files' : [ 'README.md' ],
                },
            },
        ]

        mock_manifest_paths = ['file1', 'file2']
        mock_project_root = 'some/path'
        with unittest.mock.patch( 'distribution_package.utility.multiple_manifests.open' ), \
             unittest.mock.patch( 'distribution_package.utility.multiple_manifests.yaml.load',
                                  side_effect = [ [ expected_yaml_data[ 0 ] ],
                                                  [ expected_yaml_data[ 1 ] ] ] ), \
             unittest.mock.patch.object( distribution_package.utility.multiple_manifests.Manifest,
                                         'from_yamlData' ) as \
                mock_yamlData :
            combine_manifests(mock_manifest_paths, mock_project_root)

            mock_yamlData.assert_called_once_with( expected_yaml_data, mock_project_root )
