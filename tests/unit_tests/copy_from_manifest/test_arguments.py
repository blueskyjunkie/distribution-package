#
# Copyright 2019 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

import unittest

from distribution_package.copy_from_manifest.options import UserOptions


class TestUserOptions( unittest.TestCase ) :

    def test_defaults(self) :
        arguments_input = list()

        under_test = UserOptions()
        under_test.parse_arguments( arguments_input )

        self.assertEqual( '.', under_test.project_root )
        self.assertEqual( [ 'manifest.yml' ], under_test.manifest_path )
        self.assertEqual( 'manifestOutput', under_test.output )


    def test_long_output(self) :
        arguments_input = [
            '--output',
            'some/path',
        ]

        under_test = UserOptions()
        under_test.parse_arguments( arguments_input )

        self.assertEqual( arguments_input[ 1 ], under_test.output )


    def test_short_output(self) :
        arguments_input = [
            '-o',
            'some/path',
        ]

        under_test = UserOptions()
        under_test.parse_arguments( arguments_input )

        self.assertEqual( arguments_input[ 1 ], under_test.output )


    def test_long_project_root(self) :
        arguments_input = [
            '--project-root',
            'some/path',
        ]

        under_test = UserOptions()
        under_test.parse_arguments( arguments_input )

        self.assertEqual( arguments_input[ 1 ], under_test.project_root )


    def test_short_project_root(self) :
        arguments_input = [
            '-r',
            'some/path',
        ]

        under_test = UserOptions()
        under_test.parse_arguments( arguments_input )

        self.assertEqual( arguments_input[ 1 ], under_test.project_root )


    def test_long_manifests(self) :
        arguments_input = [
            '--manifests',
            'path/file1',
            'path/file2',
        ]

        under_test = UserOptions()
        under_test.parse_arguments( arguments_input )

        self.assertEqual( arguments_input[ 1 :3 ], under_test.manifest_path )


    def test_short_manifests(self) :
        arguments_input = [
            '-m',
            'path/file1',
            'path/file2',
        ]

        under_test = UserOptions()
        under_test.parse_arguments( arguments_input )

        self.assertEqual( arguments_input[ 1 :3 ], under_test.manifest_path )
