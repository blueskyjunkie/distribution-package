#
# Copyright 2018 Russell Smiley
#
# This file is part of packager.
#
# packager is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# packager is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with packager.  If not, see <http://www.gnu.org/licenses/>.
#

"""
Test user specified packaging options.
"""

import os
import unittest

from distribution_package.make_package.options import UserOptions


class TestPackageOptions(unittest.TestCase):

    def setUp(self):
        self.options_under_test = UserOptions()

    def test_defaults(self):
        self.options_under_test.parse_arguments(list())

        self.assertTrue(self.options_under_test.is_tar_file)
        self.assertFalse(self.options_under_test.is_zip_file)

    def test_tar_package_path_enabled(self):
        self.options_under_test.parse_arguments(['-o', 'some/path', '-p', 'package-name'])

        expected_path = os.path.join(self.options_under_test.output,
                                     '{0}.tar.gz'.format(self.options_under_test.package
                                                         ))

        self.assertEqual(expected_path, self.options_under_test.tar_package_path)

    def test_tar_package_path_disabled(self):
        self.options_under_test.parse_arguments(['--no-tar'])

        self.assertIsNone(self.options_under_test.tar_package_path)

    def test_zip_package_path_enabled(self):
        self.options_under_test.parse_arguments(['-o', 'some/path', '-p', 'package-name', '--zip'])

        expected_path = os.path.join(self.options_under_test.output,
                                     '{0}.zip'.format(self.options_under_test.package))

        self.assertEqual(expected_path, self.options_under_test.zip_package_path)

    def test_zip_package_path_disabled(self):
        self.options_under_test.parse_arguments(list())

        self.assertIsNone(self.options_under_test.zip_package_path)


if __name__ == '__main__':
    unittest.main()
