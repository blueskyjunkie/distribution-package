import argparse
import typing


class UserOptions:
    DEFAULT_MANIFEST_FILE: str
    DEFAULT_OUTPUT_DIR: str
    DEFAULT_PROJECT_ROOT_DIRECTORY: str

    __parsed_arguments: typing.Optional[argparse.Namespace]
    __parser: argparse.ArgumentParser

    def __init__(self): ...

    def __getattr__(self, item: str) -> typing.Any: ...

    def parse_arguments(self, command_line_arguments: typing.List[str]): ...

    #
    # These properties are fulfilled using the ``__getattr__`` method. They are described here as
    # properties to
    # assist with PyCharm type hinting.
    #
    @property
    def manifest_path(self) -> typing.List[str]: ...

    @property
    def output(self) -> str: ...

    @property
    def project_root(self) -> typing.str: ...
