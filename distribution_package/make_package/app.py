#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""Flit entry point and main execution path for make_package utility."""

import sys
import typing

from ..utility import combine_manifests

from .options import UserOptions
from .package import Package


def main(command_line_arguments: typing.List[str]):
    """Implement the main execution path of the ``make_package`` utility."""
    user_options = UserOptions()
    user_options.parse_arguments(command_line_arguments)

    this_manifest = combine_manifests(user_options.manifest_path, user_options.project_root)

    this_package = Package(this_manifest, user_options)
    this_package.build()


def entrypoint():
    """Flit entry point to the ``make_package`` utility."""
    main(sys.argv[1:])


if __name__ == '__main__':
    entrypoint()
