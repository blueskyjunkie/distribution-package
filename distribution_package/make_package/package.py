#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""Apply manifest and user options to package building."""

from packageManifest import Manifest

from .options import UserOptions
from .tar_package import TarPackage
from .zip_package import ZipPackage


class Package:
    """Apply manifest and user options to package building."""

    def __init__(self, manifest: Manifest, user_options: UserOptions):
        """Collect manifest specification and user options for a package build."""
        self.manifest = manifest
        self.user_options = user_options
        self.packages = list()

    def build(self):
        """Generate the user configured tar and/or zip file packages."""
        if self.user_options.is_zip_file:
            package = ZipPackage(self.user_options.zip_package_path,
                                 self.user_options.project_root,
                                 self.user_options.append)
            self.packages.append(package)

        if self.user_options.is_tar_file:
            package = TarPackage(self.user_options.tar_package_path,
                                 self.user_options.project_root,
                                 self.user_options.append)
            self.packages.append(package)

        files_to_package = self.manifest.apply()
        for this_package in self.packages:
            this_package.build(files_to_package)
