#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""``TarPackage`` implementation."""

import gzip
import io
import logging
import os
import tarfile
import typing

from .base import PackageBase
from .directory import ChangeDirectory
from .interface import PackageInterface

log = logging.getLogger(__name__)


class TarPackage(PackageInterface, PackageBase):
    """Coordinate building a tar package from specified files."""

    def __init__(self, file_name: str, project_root: str, append: bool):
        """Instantiate a tar file package builder object."""
        super().__init__(file_name, project_root, append)

    def build(self, files_to_package: typing.List[str]):
        """Build the tar package using the specified files."""
        if self.append and os.path.isfile(self.file_name):
            self.__open_for_append(files_to_package)
        else:
            self.__open_for_write(files_to_package)

    def __open_for_append(self, files_to_package: typing.List[str]):
        log.info('Appending to existing file, {0}'.format(self.file_name))

        buffer = io.BytesIO()
        with gzip.open(self.file_name) as this_gzip:
            buffer.write(this_gzip.read())

        buffer.seek(0)

        with tarfile.open(fileobj=buffer,
                          mode='a') as thisFile:
            self.__do_build(files_to_package, thisFile)

        buffer.seek(0)
        with gzip.open(self.file_name, 'wb') as this_gzip:
            this_gzip.write(buffer.read())

    def __open_for_write(self, files_to_package: typing.List[str]):
        log.info('Opening or creating file for write, {0}'.format(self.file_name))
        with tarfile.open(name=self.file_name,
                          mode='w:gz') as thisFile:
            self.__do_build(files_to_package, thisFile)

    def __do_build(self, files_to_package: typing.List[str], this_tarfile):
        """Temporarily change into the project directory and add the specified files to the package."""
        with ChangeDirectory(self.project_root):
            for this_file in files_to_package:
                if os.path.isfile(this_file):
                    this_tarfile.add(this_file)
                elif not os.path.exists(this_file):
                    real_file_name = os.path.realpath(
                        os.path.abspath(os.path.join(self.project_root, this_file)))

                    log.warning('File to package does not exist, {0}'.format(real_file_name))
