#
# Copyright 2018 Russell Smiley
#
# This file is part of distribution_package.
#
# distribution_package is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# distribution_package is distributed in the hope that it will be useful
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with distribution_package.  If not, see <http://www.gnu.org/licenses/>.
#

"""``ZipPackage`` implementation."""

import os
import logging
import typing
import zipfile

from .base import PackageBase
from .directory import ChangeDirectory
from .interface import PackageInterface

log = logging.getLogger(__name__)


class ZipPackage(PackageInterface, PackageBase):
    """Coordinate building a zip package from specified files."""

    def __init__(self, file_name: str, project_root: str, append: bool):
        """Instantiate a zip file packaging object."""
        super().__init__(file_name, project_root, append)

    def build(self, files_to_package: typing.List[str]):
        """Build zip file package."""
        if self.append and os.path.isfile(self.file_name):
            self.__open_for_append(files_to_package)
        else:
            self.__open_for_write(files_to_package)

    def __open_for_append(self, files_to_package: typing.List[str]):
        log.info('Appending to existing file, {0}'.format(self.file_name))
        with  zipfile.ZipFile(self.file_name,
                              mode='a') as this_zipfile:
            self.__do_build(files_to_package, this_zipfile)

    def __open_for_write(self, files_to_package: typing.List[str]):
        with zipfile.ZipFile(self.file_name,
                             mode='w') as this_zipfile:
            self.__do_build(files_to_package, this_zipfile)

    def __do_build(self, files_to_package: typing.List[str], this_zipfile: zipfile.ZipFile):
        with ChangeDirectory(self.project_root):
            for this_file in files_to_package:
                if os.path.isfile(this_file):
                    this_zipfile.write(this_file)
                elif not os.path.exists(this_file):
                    real_file_name = os.path.realpath(
                        os.path.abspath(os.path.join(self.project_root, this_file)))

                    log.warning('File to package does not exist, {0}'.format(real_file_name))
